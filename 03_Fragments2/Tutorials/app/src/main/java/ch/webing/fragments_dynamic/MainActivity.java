package ch.webing.fragments_dynamic;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements FirstFragment.IFirstFragmentEvents {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .replace(R.id.MainActivity_placeholder_fragment1, new FirstFragment())
                    .replace(R.id.MainActivity_placeholder_fragment2, new SecondFragment())
                    .commit();
        }
    }

    @Override
    public void addNewEntry(String txt) {
        SecondFragment secondFragment = new SecondFragment();
        Bundle bundle = new Bundle();
        bundle.putString("newEntry", txt);
        secondFragment.setArguments(bundle);

        getFragmentManager().beginTransaction()
                .replace(R.id.MainActivity_placeholder_fragment2, secondFragment)
                .commit();
    }
}
