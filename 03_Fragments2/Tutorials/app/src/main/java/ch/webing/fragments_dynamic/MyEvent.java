package ch.webing.fragments_dynamic;


// I USE THIS POJO ONLY FOR EVENTBUS
public class MyEvent {
    private final String message;

    public MyEvent(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
