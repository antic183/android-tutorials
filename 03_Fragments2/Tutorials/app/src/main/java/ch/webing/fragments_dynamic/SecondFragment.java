package ch.webing.fragments_dynamic;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

/*import de.greenrobot.event.EventBus;*/

public class SecondFragment extends Fragment {

    private TextView txtNewEntry;

    public SecondFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_second, container, false);

        txtNewEntry = (TextView) view.findViewById(R.id.SecondFragment_txtNewEntry);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            String txt = bundle.getString("newEntry", "");
            Toast.makeText(getActivity(), "Der neue Eintrag = " + txt, Toast.LENGTH_SHORT).show();
            txtNewEntry.setText(txt);
        }

        /*// register your class for eventbus
        EventBus.getDefault().register(this);*/
        return view;
    }

   /* // msg from eventbus...
    public void onEvent(MyEvent event) {
        Toast.makeText(getActivity(), event.getMessage(), Toast.LENGTH_SHORT).show();
    }*/
}
