package ch.webing.fragments_dynamic;


import android.content.Context;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

/*import de.greenrobot.event.EventBus;*/

public class FirstFragment extends Fragment {
    IFirstFragmentEvents activity;
    EditText txtInputNewInput;
    Button btnAddNewInput;

    public FirstFragment() {
    }

    interface IFirstFragmentEvents {
        public void addNewEntry(String txt);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_first, container, false);
        txtInputNewInput = (EditText) view.findViewById(R.id.FirstFragment_txtInputNewEntry);
        btnAddNewInput = (Button) view.findViewById(R.id.FirstFragment_btnAddNewEntry);

        btnAddNewInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.getId() == R.id.FirstFragment_btnAddNewEntry) {
                    String newInput = txtInputNewInput.getText().toString();
                    if (!newInput.equals("")) {
                        /*// for eventbus:
                        // 1. delete existing solution with interface
                        // 2. import in app gradle: compile 'de.greenrobot:eventbus:2.4.0'
                        // 3. create your Event in pojo Class example "MyEvent".
                        EventBus.getDefault().post(new MyEvent(newInput));*/
                        activity.addNewEntry(newInput);
                        // clear input
                        txtInputNewInput.setText(null);
                        // hide software keyboard
                        try {
                            InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                            inputManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
                        } catch (Exception e) {

                        }
                    }
                }
            }
        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            activity = (IFirstFragmentEvents) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement IFirstFragmentEvents");
        }
    }
}
