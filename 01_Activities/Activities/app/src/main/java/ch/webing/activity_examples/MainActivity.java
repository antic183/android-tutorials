package ch.webing.activity_examples;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private EditText txtInputToPermit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtInputToPermit = (EditText) findViewById(R.id.txtInputToPermit);
    }

    public void executeExplizitIntent(View view) {
        if (view.getId() == R.id.btnGoToSecondActivity) {
            String txtToPermit = "";
            if (!(txtToPermit = txtInputToPermit.getText().toString()).equals("")) {
                Intent intent = new Intent(view.getContext(), SecondActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("txtFromMainActivity", txtToPermit);
                intent.putExtras(bundle);
                startActivity(intent);
            } else {
                Toast.makeText(view.getContext(), "Please enter your Message for the secondActivity...", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void executeImplizitIntent(View view) {
        Intent intent = null;
        switch (view.getId()) {
            case R.id.btnImplizitIntentCallPhoneNumber:
                intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:+41620001122"));
                break;
            case R.id.btnImplizitIntentCallSms:
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse("smsto:+41620001122"));
                intent.putExtra("sms_body", "This is your message body");
                break;
            default:
                Toast.makeText(view.getContext(), "action undefined !", Toast.LENGTH_SHORT);
        }
        startActivity(intent);
    }

}
