package ch.webing.activity_examples;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        if (savedInstanceState == null) {
            Bundle bundle = getIntent().getExtras();
            String permittedText = bundle.getString("txtFromMainActivity").toString();
            if (!permittedText.equals("")) {
                Toast.makeText(getApplicationContext(), permittedText, Toast.LENGTH_LONG).show();
            }
        }
    }
}
