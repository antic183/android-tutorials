package ch.webing.listviewrecycler;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    /*
    * INFO:
    * zuerst unter apps im build.gradle import vollziehen:
    * compile 'com.android.support:recyclerview-v7:25.3.+'
    *
    * */

    private List<Todo> todoList = new ArrayList<>();

    public MainActivity() {
        todoList.add(new Todo(1, "1203asdf"));
        todoList.add(new Todo(2, "054d50dsfsdf"));
        todoList.add(new Todo(3, "aaaa40d504as"));
        todoList.add(new Todo(4, "ssdasd105155"));
        todoList.add(new Todo(5, "1202157777xxxxx"));
        todoList.add(new Todo(76, "aa777a7a7a"));
        todoList.add(new Todo(86, "aa777a7a7a"));
        todoList.add(new Todo(96, "aa777a7a7a"));
        todoList.add(new Todo(106, "aa777a7a7a"));
        todoList.add(new Todo(116, "aa777a7a7a"));
        todoList.add(new Todo(126, "aa777a7a7a"));
        todoList.add(new Todo(136, "aa777a7a7a"));
        todoList.add(new Todo(146, "aa777a7a7a"));
        todoList.add(new Todo(156, "aa777a7a7a"));
        todoList.add(new Todo(166, "aa777a7a7a"));
        todoList.add(new Todo(177, "aa777a7a7a"));
    }

    private static class MyViewHolder extends RecyclerView.ViewHolder {
        public final TextView mTextViewId;
        public final TextView mTextViewName;

        public MyViewHolder(View view) {
            super(view);
            this.mTextViewId = (TextView) view.findViewById(R.id.myRow_id);
            this.mTextViewName = (TextView) view.findViewById(R.id.myRow_name);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.todoRecycler);
        recyclerView.setHasFixedSize(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        /*// für 2 reihige darstellung:
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));*/

        recyclerView.setAdapter(new RecyclerView.Adapter() {
            @Override
            public int getItemCount() {
                System.out.println("size = " + todoList.size());
                return todoList.size();
            }

            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = getLayoutInflater().inflate(R.layout.my_list_item, parent, false);
                return new MyViewHolder(view);
            }

            @Override
            public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
                Todo currentTodo = todoList.get(position);
                System.out.println("currentTodo = " + currentTodo.getId() + ", " + currentTodo.getMsg());

                MyViewHolder myViewHolder = (MyViewHolder) holder;

                myViewHolder.mTextViewId.setText(currentTodo.getId() + "");
                myViewHolder.mTextViewName.setText(currentTodo.getMsg());
            }
        });
    }
}
