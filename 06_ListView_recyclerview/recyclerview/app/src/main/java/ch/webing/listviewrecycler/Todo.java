package ch.webing.listviewrecycler;

/**
 * Created by antic-software-ing on 02.07.2017.
 */

public class Todo {
    private int id;
    private String msg;

    public Todo(int id, String msg) {
        this.id = id;
        this.msg = msg;
    }

    public int getId() {
        return id;
    }

    public String getMsg() {
        return msg;
    }
}
