package ch.webing.listviewsimple;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {
    private ListView myListView;

    private String[] modelForListView = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        myListView = (ListView) findViewById(R.id.my_list_view);

//        myListView.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1 , modelForListView));
        // eigenes list item
        myListView.setAdapter(new ArrayAdapter<String>(this, R.layout.my_list_item , modelForListView));
    }
}
