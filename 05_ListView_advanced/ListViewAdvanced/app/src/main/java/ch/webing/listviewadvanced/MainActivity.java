package ch.webing.listviewadvanced;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ListView myListView;
    private List<Person> persons = new ArrayList<>();
    private OwnAdapter ownAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        myListView = (ListView) findViewById(R.id.my_list_view);

        ownAdapter = new OwnAdapter(this, populatePersonsListWithDummyData());
        myListView.setAdapter(ownAdapter);
        System.out.println("--->" + ownAdapter.getCount());

        // remove item bei click darauf
        myListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
                Person clickedPerson = (Person) adapter.getItemAtPosition(position);
                persons.remove(clickedPerson);
                // zwingend ansonsten fehler
                ownAdapter.notifyDataSetChanged();
            }
        });
    }

    private List<Person> populatePersonsListWithDummyData() {
        // populate person list with dummy data:
        persons.add(new Person(1, "Markus", R.drawable.a1));
        persons.add(new Person(2, "Peter", R.drawable.a2));
        persons.add(new Person(3, "Ursina", R.drawable.a3));
        persons.add(new Person(4, "Patrizia", R.drawable.a4));
        persons.add(new Person(5, "Barbara", R.drawable.a5));
        persons.add(new Person(6, "Simona", R.drawable.a6));
        persons.add(new Person(7, "Klara", R.drawable.a7));
        persons.add(new Person(8, "a", R.drawable.a1));
        persons.add(new Person(9, "b", R.drawable.a2));
        persons.add(new Person(10, "c", R.drawable.a3));
        persons.add(new Person(11, "d", R.drawable.a4));
        persons.add(new Person(12, "e", R.drawable.a5));
        persons.add(new Person(13, "f", R.drawable.a6));
        persons.add(new Person(14, "g", R.drawable.a7));
        persons.add(new Person(15, "h", R.drawable.a1));
        persons.add(new Person(16, "i", R.drawable.a2));
        return persons;
    }
}
