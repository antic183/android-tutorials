package ch.webing.listviewadvanced;

import java.util.List;

public class Person {
    private final int id;
    private final String name;
    private final int grafik;

    public Person(int id, String name, int grafik) {
        this.id = id;
        this.name = name;
        this.grafik = grafik;
    }

    public String getName() {
        return name;
    }

    public int getGrafik() {
        return grafik;
    }

    public int getId() {
        return id;
    }
}
