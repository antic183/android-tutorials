package ch.webing.listviewadvanced;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by antic-software-ing on 02.07.2017.
 */

public class OwnAdapter extends BaseAdapter {

    private List<Person> persons;
    private MainActivity activity;

    public OwnAdapter(MainActivity activity, List<Person> persons) {
        this.activity = activity;
        this.persons = persons;
    }

    @Override
    public int getCount() {
        return persons.size();
    }

    @Override
    public Object getItem(int position) {
        return persons.get(position);
    }

    @Override
    public long getItemId(int position) {
        return persons.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View myRow = convertView == null ?
                layoutInflater.inflate(R.layout.own_person_item, parent, false)
                : convertView;

        Person currentperson = persons.get(position);
        // befülle das bild und den namen der aktuellen zeile
        ImageView imageView = (ImageView) myRow.findViewById(R.id.ownPersonItem_image);
        imageView.setImageResource(currentperson.getGrafik());
        TextView textView = (TextView) myRow.findViewById(R.id.ownPersonItem_name);
        textView.setText(currentperson.getName());

        return myRow;
    }
}
